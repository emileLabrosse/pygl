# pygl (python graphics library)

# AUTHOR: Emile Labrosse
# AUTHOR'S EMAIL: emile.labrosse@gmail.com elabrosse@rodeofx.com
# START DATE: August 18 2018

Append the /pygl/python/ directory to your PYTHONPATH to be able to import pygl.

objects:

- pygl.X:
    Represents a coordinate along the X axis which represents the horizontal direction.
    Holds a "value" attribute of type int.

- pygl.Y:
    Represents a coordinate along the Y axis which represents the vertical direction.
    Holds a "value" attribute of type int.

- pygl.Pixel
    Represents a position in 2D space.

- pygl.Line
- pygl.Triangle
- pygl.Image