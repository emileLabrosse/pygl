"""
Rez <3
"""

name = "pygl"

version = "0.0.1"

authors = ['<Emile Labrosse> emile.labrosse@gmail.com']

description = """
Python graphics library
"""

requires = [
    "python<3",
]

uuid = "34189609-220c-4307-8ac8-8297198e6005"


def commands():
    env.PYTHONPATH.append("{root}/python")
