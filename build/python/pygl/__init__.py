from pygl.axis import X
from pygl.axis import Y
from pygl.point import Point
from pygl.line import Line
from pygl.canvas import Canvas

__doc__ = """
The pygl module exposes a set of objects that can be assembled together to generate computer graphics.
Author: Emile Labrosse <emile.labrosse@gmail.com>
"""