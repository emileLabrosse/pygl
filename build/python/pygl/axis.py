from pygl.abstract import Abstract


class Axis(Abstract):
    """Abstract Axis object"""

    def __init__(self, value=0, name='axis_object'):
        """
        :param int value: value
        """
        super(Axis, self).__init__(name)
        self.value = value


class X(Axis):
    """X Axis object"""

    def __init__(self, value=0, name='x_object'):
        """
        :param int value: value
        """
        super(X, self).__init__(value, name)


class Y(Axis):
    """Y Axis object"""

    def __init__(self, value=0, name='y_object'):
        """
        :param int value: value
        """
        super(Y, self).__init__(value, name)
