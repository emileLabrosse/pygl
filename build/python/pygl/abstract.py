class Abstract(object):

    def __init__(self, name='abstract_object'):
        self.name = name
        self.str_map = {'name': self.name}

    def __str__(self):
        return "<'{0}' name: '{1}'>".format(self.__class__, self.name)

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, item):
        return self.str_map[item.lower()]

    def __setitem__(self, key, value):
        self.str_map[key.lower()] = value
