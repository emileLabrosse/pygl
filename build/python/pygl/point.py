from pygl.abstract import Abstract
from pygl.axis import X, Y


class Point(Abstract):
    """
    Generic point object to represent a coordinate in 2D space.
    """
    def __init__(self, x=0, y=0, name='point_object', color=1):
        """
        :param Axis x: Initial x value.
        :param Axis y: Initial y value.
        :param str name: Initial name.
        """
        super(Point, self).__init__(name)
        self.x = X(x)
        self.y = Y(y)
        self.color = color
        self.str_map.update({'x': self.x.value, 'y': self.y.value})
        self.axis_map = {X: self.x, Y: self.y}

    def __str__(self):
        """
        :return:
        """
        return "<class '{0}' name: '{1}' [X=({2}) Y=({3})]>".format(self.__class__,
                                                                    self.name,
                                                                    self.x.value,
                                                                    self.y.value)

    def position(self):
        """
        :return:
        """
        return tuple((self.x.value, self.y.value))

    def set_position(self, x, y):
        """
        :param int x:
        :param int y:
        """

        self.x.value = x
        self.y.value = y

    def move(self, value, axis):
        """
        :param int value:
        :param direction:
        :type direction: :class:`objects._axis.Axis`
        :return:
        """
        self.axis_map[axis].value += value

    def rotate(self, origin, value):
        """
        :param origin:
        :type origin:
        :param float value:
        :return:
        """
        raise NotImplementedError

    def scale(self, scale_factor, axis, origin=None):
        """
        :param axis:
        :type axis: :class:`objects._axis.Axis`
        :param int scaleFactor:
        :param origin:
        :type origin: :class:`objects._point.Point`
        """
        if not origin:
            origin = Point(name='origin')

        if not isinstance(origin, Point):
            raise TypeError('Invalid origin type provided. Expected: ({0}), got: ({1})'
                            .format(self.__class__, type(origin)))

        self.axis_map[axis].value = (self.axis_map[axis].value - origin.axis_map[axis].value) * scale_factor
