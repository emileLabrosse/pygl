import math

from pygl.point import Point
from pygl.abstract import Abstract


class Line(Abstract):
    """
    """

    def __init__(self, point_a=Point(x=0, y=0), point_b=Point(x=0, y=0), name='line_object'):
        """
        :param point_a:
        :param point_b:
        :param name:
        """
        super(Line, self).__init__(name)
        self.point_a = point_a
        self.point_b = point_b
        self._points_needs_update = True
        self._points = None
        self._distance = None
        self.str_map.update({'point_a': self.point_a, 'point_b': self.point_b})

    def distance(self):
        """Returns the distance between self.point_a and self.point_b
        :return:
        """
        x2 = (self.point_a.x.value - self.point_b.x.value)**2
        y2 = (self.point_a.y.value - self.point_b.y.value)**2
        return int(math.sqrt(x2 + y2))

    def _middle_point(self):
        """
        :return:
        """
        middle_x = (self.point_a.x.value + self.point_b.x.value) / 2
        middle_y = (self.point_a.y.value + self.point_b.y.value) / 2
        return Point(x=middle_x, y=middle_y)

    def _compute_points(self):
        """
        :return:
        """
        self._points_needs_update = False
        points = [self.point_a, self.point_b]
        distance = self.distance()
        for i in range(distance):
            x1 = int(self.point_a.x.value + ((i / float(distance)) * (self.point_b.x.value - self.point_a.x.value)))
            y1 = int(self.point_a.y.value + ((i / float(distance)) * (self.point_b.y.value - self.point_a.y.value)))
            points.append(Point(x=x1, y=y1))
        return points

    @property
    def points(self):
        if self._points_needs_update:
            self._points = self._compute_points()
        return self._points
