import os

from pygl.abstract import Abstract
from pygl.axis import X, Y


class Canvas(Abstract):
    """
    Generic Canvas object to abstract the process of writing .pbm files.
    """
    def __init__(self, point_list=None, width=48, height=48, bg_color=0, name='canvas_object'):
        """
        :param point_list:
        :param width:
        :param height:
        :param bg_color:
        :param name:
        """
        super(Canvas, self).__init__(name)
        self.point_list = point_list
        if not self.point_list:
            self.point_list = []
        self.height = Y(width)
        self.width = X(height)
        self.bg_color = bg_color
        self._buffer = None
        self._buffer_needs_update = True
        self.str_map.update({'height': self.height,
                             'width': self.width,
                             'bg_color': self.bg_color,
                             'point_list': self.point_list})

    def __enter__(self):
        """
        :return:
        """
        raise NotImplementedError

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        raise NotImplementedError

    def _compute_buffer(self):
        """
        :return:
        """
        self._buffer_needs_update = False
        buffer = []
        for y in range(self.height.value):
            line = ''
            for x in range(self.width.value):
                coordinates = (x, y)
                pixel_color = self.pixel_color(coordinates)
                line = '{0} {1}'.format(line, pixel_color)
            buffer.append(line)
        return buffer

    def add(self, point_list):
        """
        :param point_list:
        :return:
        """
        # TODO: It should be the purpose of the add method to convert the incoming objects
        #  TODO: to a list of points, so that the buffer methods can be only concerned about points.
        self._buffer_needs_update = True
        for point in point_list:
            self.point_list.append(point)

    def remove(self, coordinates):
        """
        :param coordinates:
        :return:
        """
        self._buffer_needs_update = True
        index = 0
        for point in self.point_list:
            if point.position() == coordinates:
                self.point_list.pop(index)
                return point
            index += 1

    def pixel_color(self, coordinates):
        """
        :param coordinates:
        :return:
        """
        color = self.bg_color
        for point in self.point_list:
            if point.position() == coordinates:
                color = point.color
        return color

    def file_header(self, path):
        """.pbm files header example:
        P1
        # basename.pbm
        48 48

        :param path: path
        :return: .pbm file header string
        """
        return 'P1\n# {0}\n{1} {2}\n'.format(os.path.basename(path),
                                             self.width.value,
                                             self.height.value)

    @property
    def buffer(self):
        """
        :return:
        """
        if self._buffer_needs_update:
            self._buffer = self._compute_buffer()
        return self._buffer

    def draw(self, path):
        """
        :param str path: write path
        :return: 0 if the write was successful, 1 otherwise.
        :rtype: int
        """
        _, extension = os.path.splitext(path)
        if extension != '.pbm':
            raise ValueError('Invalid file type: ({0}), expected: .pbm'.format(extension))
        elif os.path.exists(path):
            raise ValueError('Path already exists: ({0})'.format(path))

        with open(path, 'w') as out_image:
            out_image.write(self.file_header(path))
            for row in self.buffer:
                line = ''
                for pixel in row:
                    line = '{0}{1}'.format(line, pixel)
                out_image.write('{0}\n'.format(line))

        if os.path.exists(path):
            print 'File successfully written: ({0})'.format(path)
            return 0

        print 'Something went wrong... ({0}) No such file or directory'
        return 1
