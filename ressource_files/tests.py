import os
import re
import math
import time
import importlib

import pygl
import pygl.core
import pygl.core.utils
importlib.reload(pygl)
importlib.reload(pygl.core)
importlib.reload(pygl.core.utils)

RE_VERSION = re.compile(r'\D+(?P<version>\d+).pbm')
OUT_IMAGES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'output_images')


def get_version():
    max_version = 0

    for file_name in os.listdir(OUT_IMAGES_DIR):
        version_group = re.search(RE_VERSION, file_name)
        if version_group:
            version = int(version_group.group('version'))
            if version > max_version:
                max_version = version

    return max_version + 1


def get_line():
    """
    Line drawing function example.
    """

    start_pixel = pygl.Pixel(x=0, y=0)
    end_pixel = pygl.Pixel(x=45, y=40)
    line = pygl.Line(start=start_pixel, end=end_pixel)
    return line


def get_triangle():
    """
    Triangle drawing function example.
    """
    pixel_a = pygl.Pixel(x=40, y=15)
    pixel_b = pygl.Pixel(x=8, y=24)
    pixel_c = pygl.Pixel(x=30, y=26)
    return pygl.Triangle(pixel_a=pixel_a, pixel_b=pixel_b, pixel_c=pixel_c)


def get_image():
    """
    """
    image = pygl.Image(width=150, height=48)
    image.color = 0
    return image


def test():
    my_image = get_image()
    point = pygl.Pixel(x=60, y=30)
    origin = pygl.Pixel(x=70, y=35)
    x, y = pygl.core.utils.rotate(origin.position(), point.position(), math.radians(2))
    print(f"x={x}, y={y}")
    point = pygl.Pixel(x=x, y=y)
    my_image.add((point, origin))
    my_image.draw(os.path.join(OUT_IMAGES_DIR, f'test_{get_version()}.pbm'))


# last = 1.09358787537 seconds
# new one = 0.990696907043 seconds
startTime = time.time()
test()
endTime = time.time()
print(f'drawing took: {endTime - startTime} seconds')
