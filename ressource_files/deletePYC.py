import os
import sys


def main():
    package_root = os.path.dirname(os.path.dirname(__file__))
    pygl_lib_root = os.path.join(package_root, 'python', 'pygl')

    for root, dirs, files in os.walk(pygl_lib_root):
        for file_name in files:
            path = os.path.join(root, file_name)
            name, ext = os.path.splitext(path)
            if ext == '.pyc':
                os.remove(path)


if __name__ == '__main__':
    sys.exit(main())
