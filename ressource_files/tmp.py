import os


def search_drive(token, search_path='/'):
    """
    Returns a list of paths where the token was found

    :param token:
    :param search_path:
    :return:
    """
    pathList = []
    for root, _, files in os.walk(search_path):
        for file_name in files:
            if token in file_name.lower():
                pathList.append(os.path.join(root, file_name))
    return pathList


# print search_drive('houdini')

print __path__[0]



"""

for y in height:
    line = []
    for x in width:
        coordinate = (height, width)
        if self.pixelAtCoordinate(coordinate) and not line:
            line




"""
