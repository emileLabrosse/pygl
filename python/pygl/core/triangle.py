from pygl.core.line import Line
from pygl.core.pixel import Pixel
from pygl.core.graphic import Graphic


class Triangle(Graphic):
    """
    Triangle object.
    """

    def __init__(self, pixel_a=Pixel(), pixel_b=Pixel(), pixel_c=Pixel(), fill=True):
        """
        :param pixel_a: pixel a.
        :param pixel_b: pixel b.
        :param pixel_c: pixel c.
        :type pixel_a: :class:`pygl.core.pixel.Pixel`
        :type pixel_b: :class:`pygl.core.pixel.Pixel`
        :type pixel_c: :class:`pygl.core.pixel.Pixel`

        """
        super(Triangle, self).__init__()
        self.pixel_a = pixel_a
        self.pixel_b = pixel_b
        self.pixel_c = pixel_c
        self.line_a = Line(start=self.pixel_a, end=self.pixel_b)
        self.line_b = Line(start=self.pixel_a, end=self.pixel_c)
        self.line_c = Line(start=self.pixel_c, end=self.pixel_b)
        self.fill = fill

    def width_range(self):
        ya, yb, yc = self.pixel_a.y, self.pixel_b.y, self.pixel_c.y
        max_height = max(ya, yb, yc)
        min_height = min(ya, yb, yc)
        return range(min_height, max_height + 1)

    def height_range(self):
        xa, xb, xc = self.pixel_a.x, self.pixel_b.x, self.pixel_c.x
        max_width = max(xa, xb, xc)
        min_width = min(xa, xb, xc)
        return range(min_width, max_width + 1)

    def fill_pixels(self, existing_pixels):
        pixels = ()
        existing_positions = tuple([pixel.position() for pixel in existing_pixels])
        for y in self.height_range():
            traversed_positions = []
            for x in self.width_range():
                position = (x, y)
                if position in existing_positions:
                    traversed_positions.append(position)
            if len(traversed_positions) >= 2:
                start = Pixel.from_position(traversed_positions[0], reverse=True)
                end = Pixel.from_position(traversed_positions[-1], reverse=True)
                line = Line(start=start, end=end)
                pixels += line.pixels()
        return pixels

    def pixels(self):
        """
        :return: a tuple of pixels.
        :rtype: tuple(:class:`pygl.core.pixel.Pixel`)
        """
        pixels = ()
        for line in (self.line_a, self.line_b, self.line_c):
            pixels += line.pixels()

        if self.fill:
            pixels += self.fill_pixels(pixels)

        return pixels
