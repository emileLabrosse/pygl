"""
pygl.core.utils

The mathematical implementations in this module were not written by me.
The sources are in the function's docstrings.
"""

import math


def rotate(origin, point, angle):
    """
    2 rotation function.
    Source: https://stackoverflow.com/questions/34372480/rotate-point-about-another-point-in-degrees-python

    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return int(qx), int(qy)


def brensenham(start, end):
    """
    Returns a tuple of tuples that represent a straight line drawn between self.start and self.end.
    This is a python implementation of the bresenham line rasterization algorithm.
    Source: http://www.roguebasin.com/index.php?title=Bresenham%27s_Line_Algorithm

    :param tuple start: (x, y)
    :param tuple end: (x, y)
    :return: a tuple of coordinates.
    :rtype: tuple()
    """
    x1, y1 = start[0], start[1]
    x2, y2 = end[0], end[1]

    dx = x2 - x1
    dy = y2 - y1

    is_steep = abs(dy) > abs(dx)

    if is_steep:
        x1, y1 = y1, x1
        x2, y2 = y2, x2

    swapped = False

    if x1 > x2:
        x1, x2 = x2, x1
        y1, y2 = y2, y1
        swapped = True

    dx = x2 - x1
    dy = y2 - y1

    error = int(dx / 2.0)
    ystep = 1 if y1 < y2 else -1
    y = y1

    pixels = []
    for x in range(x1, x2 + 1):
        pixel = (x, y) if is_steep else (y, x)
        pixels.append(pixel)
        error -= abs(dy)
        if error < 0:
            y += ystep
            error += dx

    if swapped:
        pixels.reverse()

    return tuple(pixels)
