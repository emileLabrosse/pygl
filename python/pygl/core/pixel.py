"""
pygl.core.pixel"""
import math

import pygl.core.utils
from pygl.core.graphic import Graphic


class Pixel(Graphic):
    """
    Pixel object.
    """

    def __init__(self, x=0, y=0):
        """
        :param x: initial x value.
        :param y: initial y value.
        """
        super(Pixel, self).__init__()
        self.x = x
        self.y = y

    def pixels(self):
        """
        :return: tuple([self]).
        :rtype: tuple(:class:`pygl.core.pixel.Pixel`)
        """
        return tuple([self])

    def position(self):
        """
        Convenience method.

        :return: the pixels's coordinates as (x, y).
        :rtype: tuple(int)
        """
        return self.x, self.y

    def rotate(self, origin, angle):
        """
        Rotates self around origin.

        :param origin:
        :type origin: :class:`pygl.core.pixel.Pixel`
        :param int angle: the angle of the rotation.
        """
        angle = math.radians(angle)
        new_position = pygl.core.utils.rotate(origin.position(),
                                              self.position(),
                                              angle)
        self.x, self.y = new_position

    @staticmethod
    def from_position(position, reverse=False):
        return from_position(position, reverse)

    @staticmethod
    def from_positions(positions, reverse=False):
        return from_positions(positions, reverse)


def from_position(position, reverse):
    x, y = position[0], position[1]
    if reverse:
        x, y = y, x
    return Pixel(x=x, y=y)


def from_positions(positions, reverse):
    pixels = []
    for position in positions:
        pixels.append(from_position(position, reverse))
    return tuple(pixels)
