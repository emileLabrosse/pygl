"""
pygl.core.line
"""

import math

import pygl.core.utils
from pygl.core.pixel import Pixel
from pygl.core.graphic import Graphic


class Line(Graphic):
    """
    Line object.
    """

    def __init__(self, start=Pixel(x=0, y=0), end=Pixel(x=1, y=1)):
        """
        :param start: start Pixel.
        :param end: end Pixel.
        :type start: :class:`pygl.core.pixel.Pixel`
        :type end: :class:`pygl.core.pixel.Pixel`
        """
        super(Line, self).__init__()
        self.start = start
        self.end = end

    def length(self):
        """
        :return: The distance in pixels between self.start and self.end.
        :rtype: int
        """
        x2 = (self.start.x - self.end.x)**2
        y2 = (self.start.y - self.end.y)**2

        return int(math.sqrt(x2 + y2))

    def middle_pixel(self):
        """
        :return: a Pixel object positioned at the center of the line.
        :rtype: :class:`pygl.core.pixel.Pixel`
        """
        return Pixel(
            x=(self.start.x + self.end.x) / 2,
            y=(self.start.y + self.end.y) / 2
        )

    def pixels(self):
        """
        :return: a tuple of pixels.
        :rtype: tuple(:class:`pygl.core.pixel.Pixel`)
        """
        pixels = []
        for coordinates in pygl.core.utils.brensenham(self.start.position(), self.end.position()):
            x, y = coordinates
            pixels.append(Pixel(x=x, y=y))

        return tuple(pixels)
