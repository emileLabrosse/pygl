"""
pygl.core.image
"""

import os

import pygl.core.utils
from pygl.core.error import PyglError
from pygl.core.graphic import Graphic


class Image(Graphic):
    """
    Image object.
    """

    def __init__(self, width=48, height=48, content=None):
        """
        :param int width: initial width.
        :param int height: initial height.
        :param tuple content: content of the image.
        """
        super(Image, self).__init__()
        self.width = width
        self.height = height
        self.content = content or ()

    def add(self, input_objects):
        """
        :param input_objects: Graphic objects to add to the content of the image.
        :type input_objects: list(:class:`pygl.core.graphic.Graphic`)
        """
        if type(input_objects) not in (list, tuple):
            raise ValueError(f'Expected list or tuple, got: {type(input_objects)}')

        self.content += tuple([obj for obj in input_objects if isinstance(obj, Graphic)])

    def remove(self, objects_to_remove):
        """
        :param objects_to_remove: Objects to remove from the image.
        :type objects_to_remove: list(:class:`pygl.core.graphic.Graphic`)
        """
        if type(objects_to_remove) not in (list, tuple):
            raise ValueError(f'Expected list or tuple, got: {type(objects_to_remove)}')

        self.content = tuple([obj for obj in self.content if obj not in objects_to_remove])

    def pixels(self):
        """
        :return: a tuple of pixels.
        :rtype: tuple(:class:`pygl.core.pixel.Pixel`)
        """
        pixels = ()
        for obj in self.content:
            pixels += obj.pixels()
        return pixels

    def pixel_color(self, pixels, coordinates):
        """
        :param tuple coordinates: ex: (x, y).
        :param tuple pixels: a tuple of Pixel objects.
        :return: the color of a pixel at a given coordinate.
        :rtype: int
        """

        color = self.color
        for pixel in pixels:
            if pixel.position() == coordinates:
                color = pixel.color
                break

        return color

    def buffer(self):
        """
        Returns the pixel buffer.

        :return: a pixel buffer string in the .PBM P1 format.
        :rtype: str
        """
        pixels = self.pixels()
        pixel_buffer = ''
        for y in range(self.height):
            line = ''
            for x in range(self.width):
                coordinates = (x, y)
                pixel_color = self.pixel_color(pixels, coordinates)
                if line:
                    line = f'{line} {pixel_color}'

                else:
                    # We don't want a whitespace at the first iteration.
                    line = f'{line}{pixel_color}'

            line += '\n'
            pixel_buffer += line

        return pixel_buffer

    def header(self, path):
        """
        :param str path: destination path.
        :return: .pbm file header string.
        :rtype: str
        """
        return f'P1\n# {os.path.basename(path)}\n{self.width} {self.height}\n'

    def draw(self, path):
        """
        :param str path: destination path.
        :return: None
        :rtype: None
        :raises PyglError: If the destination path doesn't exist after writing the file.
        """
        with open(path, 'w') as image:
            content = self.header(path) + self.buffer()
            image.write(content)

        if os.path.exists(path):
            print(f'File successfully written: ({path})')
            return

        raise PyglError(f'Something went wrong... No such file or directory: {path}')
