"""
pygl.core.graphic
"""


class Graphic(object):
    """
    Graphic object.
    """

    def __init__(self, color=1):
        """
        :param int color:
        """
        self.color = color

    def pixels(self):
        """
        """
        raise NotImplementedError
