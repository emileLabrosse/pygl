"""
Python Graphics Library
"""
from pygl.core.line import Line
from pygl.core.pixel import Pixel
from pygl.core.image import Image
from pygl.core.triangle import Triangle

__all__ = ['Pixel', 'Image', 'Line', 'Triangle']

__doc__ = """Author: Emile Labrosse <emile.labrosse@gmail.com, elabrosse@rodeofx.com>"""
